from django.contrib.auth import authenticate, get_user_model
from django.core.cache import cache
from django.test import Client, TestCase
from django.urls import resolve
from django.utils import timezone
from .views import SignUp 
from .models import Friend_Request, Person, Status, ToDoList
from .models import Person, ToDoList
from .apps import Djanguv2Config

import time


# Create your tests here.
class SigninTestJace(TestCase):
    def setUp(self):
        self.user = get_user_model().objects.create_user(username='test', password='12test12', email='test@example.com')
        self.user.save()

    def test_ajax_Jace(self):
        response = Client().get('/ajax/validate_username/?username=test')
        self.assertEqual(response.status_code, 200)

    def tearDown(self):
        self.user.delete()

    def test_correct(self):
        user = authenticate(username='test', password='12test12')
        self.assertTrue((user is not None) and user.is_authenticated)

    def test_wrong_username(self):
        user = authenticate(username='wrong', password='12test12')
        self.assertFalse(user is not None and user.is_authenticated)

    def test_wrong_pssword(self):
        user = authenticate(username='test', password='wrong')
        self.assertFalse(user is not None and user.is_authenticated)

class TestJace(TestCase):
    def test_appdjangu_name(self):
        self.assertEqual(Djanguv2Config.name, "djanguv2")

    def test_url_is_exist_login(self):
        response = Client().get('/login/') 
        self.assertEqual(response.status_code, 200)

    
        


class TestDjanguV2Jace(TestCase):
    def test_appdjangu_name(self):
        self.assertEqual(Djanguv2Config.name, "djanguv2")

    def test_url_is_exist_login(self):
        response = Client().get('') 
        self.assertEqual(response.status_code, 200)

    def test_url_is_exist_signup(self):
        response = Client().get('/signup/') 
        self.assertEqual(response.status_code, 200)


    def test_using_template_login(self):
        response = Client().get('/login/')
        response = Client().get('')
        self.assertTemplateUsed(response, 'base.html')

    def test_using_template_signup(self):
        response = Client().get('/signup/')
        self.assertTemplateUsed(response, 'signup.html')

class TestYosua(TestCase):
    def test_to_do_list_template(self):
        client = Client()
        dummy_user = get_user_model().objects.create_user(username='test', password='12test12', email='test@example.com')
        dummy_user.save()
        client.login(username='test', password='12test12')

        response = client.get('/todolist/')
        self.assertTemplateUsed(response, 'todolist.html')

    def test_to_do_list_get(self):
        client = Client()
        dummy_user = get_user_model().objects.create_user(username='test', password='12test12', email='test@example.com')
        dummy_user.save()
        client.login(username='test', password='12test12')
        
        response = client.get('/todolist/')
        self.assertEqual(response.status_code, 200)
    
    def test_add_to_do_list(self):
        client = Client()
        dummy_user = get_user_model().objects.create_user(username='test', password='12test12', email='test@example.com')
        dummy_user.save()
        client.login(username='test', password='12test12')
        
        client.post('/todolist/', {'content':'Testing'})
        self.assertEqual(ToDoList.objects.all().count(), 1)
    
    def test_delete_to_do_list(self):
        client = Client()
        dummy_user = get_user_model().objects.create_user(username='test', password='12test12', email='test@example.com')
        dummy_user.save()
        client.login(username='test', password='12test12')
        
        client.post('/todolist/', {'content':'Testing'})
        to_do_obj = ToDoList.objects.get(content='Testing')
        url_str = '/deletetodo/' + str(to_do_obj.pk)
        
        client.get(url_str)
        self.assertEqual(ToDoList.objects.all().count(), 0)
    
    def test_done_to_do_list(self):
        client = Client()
        dummy_user = get_user_model().objects.create_user(username='test', password='12test12', email='test@example.com')
        dummy_user.save()
        client.login(username='test', password='12test12')

        client.post('/todolist/', {'content':'Testing'})
        to_do_obj = ToDoList.objects.get(content='Testing')
        url_str = '/donetodo/' + str(to_do_obj.pk)
        client.get(url_str)

        to_do_obj = ToDoList.objects.get(content='Testing')
        self.assertTrue(to_do_obj.completed)

    def test_undone_to_do_list(self):
        client = Client()
        dummy_user = get_user_model().objects.create_user(username='test', password='12test12', email='test@example.com')
        dummy_user.save()
        client.login(username='test', password='12test12')

        client.post('/todolist/', {'content':'Testing'})
        to_do_obj = ToDoList.objects.get(content='Testing')
        to_do_obj.completed = True
        to_do_obj.save()
        url_str = '/undonetodo/' + str(to_do_obj.pk)
        client.get(url_str)
        
        to_do_obj = ToDoList.objects.get(content='Testing')
        self.assertFalse(to_do_obj.completed)
    
    def test_to_do_list_fetch(self):
        client = Client()
        dummy_user = get_user_model().objects.create_user(username='test', password='12test12', email='test@example.com')
        dummy_user.save()
        client.login(username='test', password='12test12')
        
        client.post('/todolist/', {'content':'Testing'})
        response = client.get('/todolist/fetch/')
        
        # print(response["Location"])
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(str(response.content, encoding='utf8'), [{"content": "Testing", "completed": False, "pk": 1}])
    
    def test_search_ajax(self):
        client = Client()
        dummy_user = get_user_model().objects.create_user(username='test', password='12test12', email='test@example.com')
        dummy_user.save()
        client.login(username='test', password='12test12')

        response = client.get('/searchajax/?q=w/')
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(str(response.content, encoding='utf8'), [])
    
    def test_send_friend_request(self):
        client = Client()
        from_user = get_user_model().objects.create_user(username='test', password='12test12', email='test@example.com')
        from_user.save()
        to_user = get_user_model().objects.create_user(username='test2', password='12test12', email='test2@example.com')
        to_user.save()
        
        client.login(username='test', password='12test12')
        
        url_str = '/send_friend_request/' + str(from_user.id) + '/' + str(to_user.id) + '/'
        response = client.get(url_str)
        self.assertRedirects(response, '/searchfriends/', status_code=302, target_status_code=200, msg_prefix='', fetch_redirect_response=True)
    
    def test_accept_friend_request(self):
        client = Client()
        user1 = get_user_model().objects.create_user(username='test', password='12test12', email='test@example.com')
        user1.save()
        user2 = get_user_model().objects.create_user(username='test2', password='12test12', email='test2@example.com')
        user2.save()

        client.login(username='test', password='12test12')
        url_str = '/send_friend_request/' + str(user1.id) + '/' + str(user2.id) + '/'
        client.get(url_str)
        client.logout()

        client.login(username='test2', password='12test12')
        url_str = '/accept_friend_request/' + str(user1.id) + '/'
        response = client.get(url_str)
        self.assertEqual(response.status_code, 302)


class TestHasna(TestCase):
    def test_url_status_login(self):
        client = Client()
        dummy_user = get_user_model().objects.create_user(username='test', password='12test12', email='test@example.com')
        dummy_user.save()
        client.login(username='test', password='12test12')

        response = client.get('/isistatus/') 
        self.assertEqual(response.status_code, 200)

    # def test_template_status(self):
    #     client = Client()
    #     dummy_user = get_user_model().objects.create_user(username='test', password='12test12', email='test@example.com')
    #     dummy_user.save()
    #     client.login(username='test', password='12test12')

    #     response = client.get('/isistatus/')
    #     self.assertTemplateUsed(response, 'isistatus.html')

    def test_apakah_ada_status(self):
        client = Client()
        dummy_user = get_user_model().objects.create_user(username='test', password='12test12', email='test@example.com')
        dummy_user.save()
        client.login(username='test', password='12test12')

        response = client.get('/isistatus/') 
        html_kembalian = response.content.decode('utf8')
        self.assertIn('Status', html_kembalian)

    def test_url_test_login(self):
        client = Client()
        dummy_user = get_user_model().objects.create_user(username='test', password='12test12', email='test@example.com')
        dummy_user.save()
        client.login(username='test', password='12test12')

        response = client.get('/test/') 
        self.assertEqual(response.status_code, 200)

    def test_template_status(self):
        client = Client()
        dummy_user = get_user_model().objects.create_user(username='test', password='12test12', email='test@example.com')
        dummy_user.save()
        client.login(username='test', password='12test12')

        response = client.get('/test/')
        self.assertTemplateUsed(response, 'test.html')
        # self.assertTemplateUsed(response, 'signup.html')

class testHamzah(TestCase):
    def test_search_template(self):
        client = Client()
        dummy_user = get_user_model().objects.create_user(username='test', password='12test12', email='test@example.com')
        dummy_user.save()
        client.login(username='test', password='12test12')

        response = client.get('/searchfriends/')
        self.assertTemplateUsed(response, 'friends.html')

    def test_search_url(self):
        client = Client()
        dummy_user = get_user_model().objects.create_user(username='test', password='12test12', email='test@example.com')
        dummy_user.save()
        client.login(username='test', password='12test12')

        response = client.get('/searchfriends/')
        self.assertEqual(response.status_code, 200)

    def test_acceptfriend_template(self):
        client = Client()
        dummy_user = get_user_model().objects.create_user(username='test', password='12test12', email='test@example.com')
        dummy_user.save()
        client.login(username='test', password='12test12')

        response = client.get('/acceptfriends/')
        self.assertTemplateUsed(response, 'acceptfriends.html')

    def test_acceptfriend_url(self):
        client = Client()
        dummy_user = get_user_model().objects.create_user(username='test', password='12test12', email='test@example.com')
        dummy_user.save()
        client.login(username='test', password='12test12')

        response = client.get('/acceptfriends/')
        self.assertEqual(response.status_code, 200)

    def test_addfriend_template(self):
        client = Client()
        dummy_user = get_user_model().objects.create_user(username='test', password='12test12', email='test@example.com')
        dummy_user.save()
        client.login(username='test', password='12test12')

        client = Client()
        dummy_user = get_user_model().objects.create_user(username='test2', password='12test12', email='test@example.com')
        dummy_user.save()
        client.login(username='test2', password='12test12')

        response = client.get('/acceptfriends/')
        self.assertEqual(response.status_code, 200)

    def test_search_ajax(self):
        client = Client()
        dummy_user = get_user_model().objects.create_user(username='test', password='12test12', email='test@example.com')
        dummy_user.save()
        client.login(username='test', password='12test12')

        response = client.get('/searchajax/?q=w/')
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(str(response.content, encoding='utf8'), [])
    
    def test_send_friend_request(self):
        client = Client()
        from_user = get_user_model().objects.create_user(username='test', password='12test12', email='test@example.com')
        from_user.save()
        to_user = get_user_model().objects.create_user(username='test2', password='12test12', email='test2@example.com')
        to_user.save()
        
        client.login(username='test', password='12test12')
        
        url_str = '/send_friend_request/' + str(from_user.id) + '/' + str(to_user.id) + '/'
        response = client.get(url_str)
        self.assertRedirects(response, '/searchfriends/', status_code=302, target_status_code=200, msg_prefix='', fetch_redirect_response=True)
    
    def test_accept_friend_request(self):
        client = Client()
        user1 = get_user_model().objects.create_user(username='test', password='12test12', email='test@example.com')
        user1.save()
        user2 = get_user_model().objects.create_user(username='test2', password='12test12', email='test2@example.com')
        user2.save()

        client.login(username='test', password='12test12')
        url_str = '/send_friend_request/' + str(user1.id) + '/' + str(user2.id) + '/'
        client.get(url_str)
        client.logout()

        client.login(username='test2', password='12test12')
        url_str = '/accept_friend_request/' + str(user1.id) + '/'
        response = client.get(url_str)
        self.assertEqual(response.status_code, 302)
    
