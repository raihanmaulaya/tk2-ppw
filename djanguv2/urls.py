from django.contrib.auth import views as auth_views
from django.urls import include, path 
from . import views

app_name = 'djanguv2'

urlpatterns = [
    path('', views.index, name='home'),
    path('login/', auth_views.LoginView.as_view(template_name='login.html'),name='login'),
    path('logout/', auth_views.LogoutView.as_view(),name='logout'),
    path('signup/', views.SignUp.as_view(),name='signup'),
    path('thanks/',views.index,name='thanks'),
    path('test/',views.friend_list,name='test'),
    path('searchfriends/',views.SearchFriends,name='searchfriends'),
    path('searchajax/',views.searchajax,name='searchajax'),
    path('acceptfriends/',views.acceptfriends,name='acceptfriends'),
    path('send_friend_request/<int:userfromID>/<int:userID>/',views.send_friend_request, name='send friends request'),
    path('accept_friend_request/<int:requestID>/',views.accept_friend_request, name='accept friends request'),
    path('decline_friend_request/<int:requestID>/',views.ketolak, name='ketolak'),
    path('delete_friend/<int:requestID>',views.delete_friend, name='delete'),
    path('isistatus/', views.status, name='status'),
    path('todolist/', views.to_do_list, name='todolist'),
    path('todolist/fetch/', views.to_do_list_fetch, name='todolistfetch'),
    path('deletetodo/<int:pk>', views.delete_to_do_list, name='deletetodo'),
    path('donetodo/<int:pk>', views.done_to_do_list, name='donetodo'),
    path('undonetodo/<int:pk>', views.undone_to_do_list, name='undonetodo'),
    path('ajax/validate_username/', views.validate_username, name='validate_username'),
]