function myFunctionDark() {
    var element = document.body;
    element.classList.toggle("darkmode");
}

function myFunctionBlue() {
    var element = document.body;
    element.classList.toggle("bluemode");
}

$( "#form" ).submit(function() {
    alert( "Status kamu sudah terupload!" );
})

$(function () {
    $.ajax({
      type: 'GET',
      cache: false,
      url: location.href,
      complete: function (req, textStatus) {
        var dateString = req.getResponseHeader('Date');
        if (dateString.indexOf('GMT') === -1) {
          dateString += ' GMT';
        }
        var date = new Date(dateString);
        $('#serverTime').text(date);
      }
    })
})
